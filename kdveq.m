N=401;
%increase N to increase dx
y= zeros(1,N);
z= zeros(1,N);
x1= -100;
x2= 300;
x= linspace(x1,x2,N);
dt = 5e-3;
t0=0;
%increase nT to increase T
nT=4000;
dx=(x2-x1)/(N-1);

t0 = 0;
g=9.81;
H=2;
aw = sqrt(g*H);
b = 1.5;
sigma = H^2/6;
c= aw + 0.1*(H/2);

for l = 2:N-1
    y(l)=2*(c-aw)*sech( (1/(2*H)) * sqrt(6*((c-aw)/aw))*(x(l)-(c*t0)) )^2;
end

%change time intergration method
y=midpoint1(y,x,N,dt,dx,c,nT,aw,sigma);

tf=nT*dt;

for l = 2:N-1
    z(l)=2*(c-aw)*sech( (1/(2*H)) * sqrt(6*((c-aw)/aw))*(x(l)-(c*tf)) )^2;
end

plot(x,y,'-b',x,z,'-r');
title('Solution of KdV equation')
legend('Runge-Kutta 4','Exact');
xlabel('Node (x)') 
ylabel('Solution (y)')
total=0;
for l = 2:N-1 
    oop= (y(l)-z(l))^2;
    total = total + oop;
end

total = ((total/N))^(1/2);

function [y] = rhsfunc(x,y,N,dx,c)
    for i = 2:N-1
        y(i) = (x(i+1)-(2*x(i))+x(i-1))*(c/dx^2);
    end
end



function [r] = rhsfunc1(y,x,N,dx,c,aw,sigma)
    r= zeros(1,N);
    f= zeros(1,N);
    for k = 1:N
        f(k) = aw*y(k) + 0.75*(y(k)^2); 
    end
    for i = 3:N-2
        r(i) = ((-1/(12*dx))*(f(i-2)-8*(f(i-1))-f(i+2)+8*(f(i+1))))-((sigma/(2*(dx^3)))*(-y(i-2)+2*(y(i-1))+y(i+2)-2*(y(i+1))));
    end 
end

function [y] = rungekutta1(y,x,N,dt,dx,c,nT,aw,sigma)
    for k = 1:nT
        k1=dt*rhsfunc1(y,x,N,dx,c,aw,sigma);
        k2=dt*rhsfunc1(y+(k1/2),x,N,dx,c,aw,sigma);
        k3=dt*rhsfunc1(y+(k2/2),x,N,dx,c,aw,sigma);
        k4=dt*rhsfunc1(y+(k3),x,N,dx,c,aw,sigma);
        y = y + (k1/6) + (k2/3) + (k3/3) + (k4/6);
    end
end

function [y] = midpoint1(y,x,N,dt,dx,c,nT,aw,sigma)
    for k = 1:nT
        k1=dt*rhsfunc1(y,x,N,dx,c,aw,sigma);
        k2=dt*rhsfunc1(y+(k1/2),x,N,dx,c,aw,sigma);
        y = y + k2;
    end
end

function [y] = euler (y,x,N,dt,dx,c,nT,aw,sigma)
    for k = 1:nT
        k1= dt*rhsfunc1(y,x,N,dx,c,aw,sigma);
        y = y + k1;
    end
end