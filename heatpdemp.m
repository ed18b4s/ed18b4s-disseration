N=101;
x= linspace(0,1,N);
y= linspace(0,1,N);
z= linspace(0,1,N);
v= linspace(0,1,N);
v1= linspace(0,1,N);
v2= linspace(0,1,N);
v3= linspace(0,1,N);
v4= linspace(0,1,N);
y(1)=0;
y(101)=0;
x(1)=0;
x(101)=0;
z(1)=0;
z(101)=0;
v(101)=0;
v1(101)=0;
v2(101)=0;
v3(101)=0;
v4(101)=0;
for l = 2:N-1
    x(l)=sin(pi*x(l));
end




for l = 2:N-1
    z(l)=sin(pi*z(l))*exp(-pi^2*0.01);
end


c=1;
dt = 10^-5;
t0=0;
nT=1000;
dx=1/(N-1);

%change to change method
x= euler(x,y,N,dt,dx,c,nT);

plot(x)
function [y] = rhsfunc(x,y,N,dx,c)
    for i = 2:N-1
        y(i) = (x(i+1)-(2*x(i))+x(i-1))*(c/dx^2);
    end
end

function [x] = rungekutta(x,y,N,dt,dx,c,nT)
    for k = 1:nT
        y=rhsfunc(x,y,N,dx,c);
        v1 = x + ((dt/2)*y);
        y=rhsfunc(v1,y,N,dx,c);
        v2 = v1 + ((dt/2)*y);
        y=rhsfunc(v2,y,N,dx,c);
        v3 = v2 + (dt*y);
        y=rhsfunc(v3,y,N,dx,c);
        x = (x/6) + (v1/3) + (v2/3) + (v3/6) + (dt*y);
    end
end


function [x] = midpoint(x,y,N,dt,dx,c,nT)
    for k = 1:nT
        k1=rhsfunc(x,y,N,dx,c);
        v = x + ((dt/2)*y);
        k2=rhsfunc(v,y,N,dx,c);
        x = v + (dt*y);
    end
end

function [x] = euler (x,y,N,dt,dx,c,nT)
    for k = 1:nT
        y=rhsfunc(x,y,N,dx,c);
        x = x + (dt*y);
    end
end
